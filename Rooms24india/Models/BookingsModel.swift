//
//  BookingsModel.swift
//  Rooms24india
//
//  Created by admin on 25/08/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation

class BookingsModel: NSObject {

}

// MARK: - Welcome
struct BookingListData: Codable {
    let statusCode: Int
    let success: Bool
    let message: String
    let data: DataClassBooking
}

// MARK: - DataClass
struct DataClassBooking: Codable {
    let booking_list : [Datum]
}

// MARK: - Datum
struct Datum: Codable {
    let sid: String
    let userIDFk: Int
    let name, phoneNumber, emailAddress, checkIn: String
    let checkOut: String
    let adults, child: Int
    let createdAt, updatedAt: String
    let hotel: Hotel

    enum CodingKeys: String, CodingKey {
        case sid
        case userIDFk = "user_id_fk"
        case name, phoneNumber, emailAddress, checkIn, checkOut, adults, child
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case hotel
    }
}

// MARK: - Hotel
struct Hotel: Codable {
    let sid, hotelName, hotelDescription, hotelImage: String
    let rating: Double
    let bookingPrice, isPopular: Int
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case sid
        case hotelName = "hotel_name"
        case hotelDescription = "hotel_description"
        case hotelImage = "hotel_image"
        case rating
        case bookingPrice = "booking_price"
        case isPopular = "is_popular"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Encode/decode helpers

