//
//  BookingsList.swift
//  Rooms24india
//
//  Created by admin on 23/08/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class BookingsList: Rooms24BaseVC {

    @IBOutlet weak var bookingsTbl : UITableView!

    var bookingsData : BookingListData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")
        serverRequest()
    }
    
    
    func serverRequest()  {
        let url: String = "https://rooms24india.com/sh/api/v1/bookingList"
        WebServices.getRequest(urlApiString: url) {[weak self] (json, message, status) in
            guard let owner = self else {return}
            if status == true {
                let welcome = try? JSONDecoder().decode(BookingListData.self, from: (json.rawData()))
                self?.bookingsData = welcome
                self?.bookingsTbl.reloadData()
                
            } else {
                print(message!)
            }
            
        }
    }
    @IBAction func btnActionBack(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }

}

extension BookingsList: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookingsData?.data.booking_list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingListTblCell", for: indexPath) as! BookingListTblCell
        let hotelDetail = bookingsData?.data.booking_list[indexPath.row]
        cell.imgHotel.kf.setImage(with: URL(string: hotelDetail?.hotel.hotelImage ?? "")!)
        //cell.lblAddress.text = hotelDetail?.hotel.hotel_name
        cell.lblHotelName.text = hotelDetail?.hotel.hotelName
        cell.lblPrice.text = "₹ \(hotelDetail!.hotel.bookingPrice)"
        cell.ratings.rating = hotelDetail?.hotel.rating ?? 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 320
        
    }
}
