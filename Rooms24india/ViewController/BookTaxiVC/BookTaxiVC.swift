//
//  BookTaxiVC.swift
//  Rooms24india
//
//  Created by admin on 22/10/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class BookTaxiVC: UIViewController {
    
    
    @IBOutlet weak var lblContact : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnBookNow(_ sender: Any) {
        if lblContact.text != "" {
            Utils.showAlert(title: "", msg: "Thank You for your suggestion", selfObj: self) {
               _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
